package com.example.a19810920_mikemalekzadeh_nycschools.network

/**
 * THis is a helper class that contains the base url of the endpoint.
 */
object NetworkHelper {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
}