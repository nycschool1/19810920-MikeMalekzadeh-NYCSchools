package com.example.a19810920_mikemalekzadeh_nycschools.network.cache

/**
 * This is an interface of Cache. Basically this is a LRU cache.
 */
interface Cache {
    /**
     * Adding a key and value to the cache
     * @param key key of the cache.
     * @param value A value for the cache which is network response here.
     */
    fun add(key: String, value: Any?)

    /**
     * Get a value. If it's not available it returns null.
     * @param key key of the cache.
     */
    fun get(key: String): Any?

    /**
     * Check if a key is existed in the cache.
     * @param key key of the cache.
     */
    fun isValid(key: String): Boolean

    /**
     * Clear the the entire cache.
     */
    fun clear()
}