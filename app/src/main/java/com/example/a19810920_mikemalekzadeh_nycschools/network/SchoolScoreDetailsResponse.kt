package com.example.a19810920_mikemalekzadeh_nycschools.network

import com.google.gson.annotations.SerializedName

data class SchoolScoreDetailsResponse(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: Int,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: Int,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: Int,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: Int
)