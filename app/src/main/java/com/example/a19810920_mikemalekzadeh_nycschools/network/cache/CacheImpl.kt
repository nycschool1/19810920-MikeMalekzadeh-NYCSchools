package com.example.a19810920_mikemalekzadeh_nycschools.network.cache

/**
 * This is implementation of LRU cache. It uses HashMap and Linked list to
 * make it fast.
 */
class CacheImpl(
    private val maxCacheSize: Int
) : Cache {
    private val map = HashMap<String, LinkedListNode>()
    private var listHead: LinkedListNode? = null
    private var listTail: LinkedListNode? = null


    override fun add(key: String, value: Any?) {
        //Remove if already there.
        removeKey(key)

        //If full, remove last recently used item from cache.
        if (map.size >= maxCacheSize && listTail != null) {
            removeKey(key = listTail?.key ?: throw Exception("Key is not available"))
        }

        //Insert new Node.
        val newNode = LinkedListNode(key = key, value = value)
        insertAtFrontOfLinkedList(newNode)
        map[key] = newNode
    }

    /**
    Get value for key and mark as most recently used.
     */
    override fun get(key: String): Any? {
        return if (isValid(key)) {
            val node = map[key] ?: return null

            //Move to front of list to mark as most recently used
            if (node != listHead) {
                removeFromLinkedList(node)
                insertAtFrontOfLinkedList(node)
            }
            node.value
        } else {
            null
        }
    }

    override fun isValid(key: String): Boolean {
        return map[key] != null
    }

    private fun removeKey(key: String): Boolean {
        val node = map[key]
        removeFromLinkedList(node)
        map.remove(key)
        return true
    }

    override fun clear() {
        map.clear()
    }

    private fun removeFromLinkedList(linkedListNode: LinkedListNode?) {
        linkedListNode?.let { node ->
            if (node.prevNode != null) node.prevNode?.nextNode = node.nextNode
            if (node.nextNode != null) node.nextNode?.prevNode = node.prevNode
            if (node == listTail) listTail = node.prevNode
            if (node == listHead) listHead = node.nextNode
        }
    }

    private fun insertAtFrontOfLinkedList(node: LinkedListNode) {
        if (listHead == null) {
            listHead = node
            listTail = node
        } else {
            listHead?.prevNode = node
            node.nextNode = listHead
            listHead = node
        }
    }

    private data class LinkedListNode(
        val key: String,
        val value: Any?,
        var nextNode: LinkedListNode? = null,
        var prevNode: LinkedListNode? = null
    )
}