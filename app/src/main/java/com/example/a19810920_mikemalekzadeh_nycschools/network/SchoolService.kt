package com.example.a19810920_mikemalekzadeh_nycschools.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolService {

    @GET("s3k6-pzi2.json")
    fun getSchools(
        @Query("\$select", encoded = true) select: String,
        @Query("\$order", encoded = true) order: String,
        @Query("\$limit", encoded = true) limit: Int,
        @Query("\$offset", encoded = true) offset: Int,
    ): Single<List<SchoolResponse>>


    @GET("f9bf-2cp4.json")
    fun getSchoolScoreDetails(
        @Query("dbn") dbn: String
    ): Single<List<SchoolScoreDetailsResponse>>
}