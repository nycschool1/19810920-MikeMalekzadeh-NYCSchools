package com.example.a19810920_mikemalekzadeh_nycschools.network

import com.google.gson.annotations.SerializedName

data class SchoolResponse (
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("school_email")
    val schoolEmail: String?
)