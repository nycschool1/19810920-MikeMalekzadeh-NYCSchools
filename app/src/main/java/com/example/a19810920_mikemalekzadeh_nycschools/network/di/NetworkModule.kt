package com.example.a19810920_mikemalekzadeh_nycschools.network.di

import com.example.a19810920_mikemalekzadeh_nycschools.network.NetworkHelper
import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolService
import com.example.a19810920_mikemalekzadeh_nycschools.network.cache.Cache
import com.example.a19810920_mikemalekzadeh_nycschools.network.cache.CacheImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun getGson() = Gson()

    @Provides
    fun getOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    fun getRetrofit(gson: Gson, okHttpClient: OkHttpClient): SchoolService {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(NetworkHelper.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(SchoolService::class.java)
    }

    @Provides
    @Singleton
    fun getCache(): Cache {
        //Set the cache size to 3 just for fun.
        return CacheImpl(maxCacheSize = 3)
    }
}