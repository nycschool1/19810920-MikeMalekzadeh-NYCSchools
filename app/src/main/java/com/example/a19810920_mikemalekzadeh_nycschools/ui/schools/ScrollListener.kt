package com.example.a19810920_mikemalekzadeh_nycschools.ui.schools

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * This is Recycler view Scroll listener that capture loading while scrolling downward.
 * @param loadMore this is lambda function that is invoked when load more data is needed. Offset
 * needs to be passed in order to fetch the right data.
 */
class ScrollListener(
    private val loadMore: (offSet: Int) -> Unit
) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
        if (dy > 0 && linearLayoutManager != null) { //check for scroll down
            val visibleItemCount = linearLayoutManager.childCount
            val totalItemCount = linearLayoutManager.itemCount
            val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                // Do pagination.. i.e. fetch new data
                loadMore.invoke(totalItemCount)
            }
        }
    }
}
