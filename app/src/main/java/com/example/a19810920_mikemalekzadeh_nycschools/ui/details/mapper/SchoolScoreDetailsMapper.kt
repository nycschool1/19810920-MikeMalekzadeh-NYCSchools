package com.example.a19810920_mikemalekzadeh_nycschools.ui.details.mapper

import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolScoreDetailsResponse
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.model.SchoolScoreDetailsPresentationModel
import javax.inject.Inject

/**
 * Mapper class to map data from domain layer to presentation model.
 */
class SchoolScoreDetailsMapper @Inject constructor() {
    fun map(schoolScoreDetailsResponse: List<SchoolScoreDetailsResponse>): SchoolScoreDetailsPresentationModel {
        val satScores = schoolScoreDetailsResponse.first()
        return SchoolScoreDetailsPresentationModel(
            dbn = satScores.dbn,
            schoolName = satScores.schoolName,
            numOfSatTestTakersText = satScores.numOfSatTestTakers.toString(),
            satCriticalReadingAvgScoreText = satScores.satCriticalReadingAvgScore.toString(),
            satMathAvgScoreText = satScores.satMathAvgScore.toString(),
            satWritingAvgScoreText = satScores.satWritingAvgScore.toString()
        )
    }

}