package com.example.a19810920_mikemalekzadeh_nycschools.ui.schools

import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import dagger.hilt.android.AndroidEntryPoint
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.example.a19810920_mikemalekzadeh_nycschools.R
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.SchoolScoreDetailsActivity
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.adapter.SchoolAdapter
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model.SchoolPresentationModel
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model.SchoolUIState
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model.SchoolUIState.Loading

/**
 * This is [SchoolListActivity] that shows the school names.
 */
@AndroidEntryPoint
class SchoolListActivity : AppCompatActivity() {


    private val viewModel: SchoolViewModel by viewModels()
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var adapter: SchoolAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        observeToUiState()
    }

    private fun setupViews() {
        progressBar = findViewById(R.id.progressBar)
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView).also {
            adapter = SchoolAdapter(
                onClicked = { dbn ->
                    navigateToSchoolDetails(dbn)
                },
                onWebSiteClicked = { url ->
                    openWebUri(url)
                }
            )
            it.adapter = adapter
            // Adding scroll listener to load more schools
            it.addOnScrollListener(
                ScrollListener { offset ->
                    viewModel.loadMore(offset)
                }
            )
            // Item decoration to add space between rows.
            it.addItemDecoration(
                object : RecyclerView.ItemDecoration() {
                    override fun getItemOffsets(
                        outRect: Rect,
                        view: View,
                        parent: RecyclerView,
                        state: RecyclerView.State
                    ) {
                        outRect.bottom = resources.getDimensionPixelSize(R.dimen.recycler_view_space)
                    }
                }
            )
        }
    }

    private fun openWebUri(url: String) {
        var uri = Uri.parse(url)
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            uri = Uri.parse("http://$url");
        }
        val browserIntent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(browserIntent)
    }

    private fun navigateToSchoolDetails(dbn: String) {
        startActivity(SchoolScoreDetailsActivity.getIntent(this, dbn))
    }

    private fun observeToUiState() {
        viewModel.uiState.observe(this) { schoolUIState: SchoolUIState ->
            handleUiState(schoolUIState)
        }
    }

    private fun handleUiState(uiState: SchoolUIState) {
        when (uiState) {
            is Loading -> {
                progressBar.isVisible = true
            }
            is SchoolUIState.Error -> {
                progressBar.isVisible = false
                Toast.makeText(this, uiState.throwable.stackTraceToString(), Toast.LENGTH_LONG)
                    .show()
            }
            is SchoolUIState.Success -> {
                progressBar.isVisible = false
                renderUi(uiState.schoolPresentationModel)
            }
        }
    }

    private fun renderUi(schoolPresentationModel: SchoolPresentationModel) {
        adapter.update(schoolPresentationModel.school)
    }
}