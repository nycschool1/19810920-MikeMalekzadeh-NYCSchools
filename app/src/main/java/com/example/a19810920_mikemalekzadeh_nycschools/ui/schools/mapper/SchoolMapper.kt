package com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.mapper

import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolResponse
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model.SchoolPresentationModel
import javax.inject.Inject

/**
 * This is a mapper class to map domain model to presentation model.
 */
class SchoolMapper @Inject constructor() {
    fun map(schoolResponses: List<SchoolResponse>): SchoolPresentationModel {
        return SchoolPresentationModel(
            schoolResponses.map {
                SchoolPresentationModel.School(
                    dbn = it.dbn,
                    schoolName = it.schoolName,
                    city = it.city,
                    website = it.website,
                    schoolEmail = it.schoolEmail.orEmpty()
                )
            }
        )
    }

}