package com.example.a19810920_mikemalekzadeh_nycschools.ui.schools

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a19810920_mikemalekzadeh_nycschools.repository.SchoolRepository
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.mapper.SchoolMapper
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model.SchoolUIState
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * This is a ViewModel that is used in SchoolListActivity and it
 * request data from network and make presentation model and uses live data to
 * post the UI state and it's presentation model.
 * @param schoolRepository Repository to fetch the schools.
 * @param schoolMapper Making presentation model from domain model or network response.
 */
@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val schoolRepository: SchoolRepository,
    private val schoolMapper: SchoolMapper
) : ViewModel() {

    /**
     * This is a disposable that keeps all observable and clear those
     * whenever the ViewModel is destroyed.
     */
    private val compositeDisposable = CompositeDisposable()

    /**
     * Using Live data to post the UI States. There UI states here
     * Loading, Error, Success.
     */
    private val _uiState = MutableLiveData<SchoolUIState>()
    val uiState: LiveData<SchoolUIState> = _uiState


    init {
        // Places initial fetch as soon as the app get launched.
        loadSchools()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    /**
     * Loads more schools while scrolling to the bottom.
     * @param offset This number of first element should be fetched.
     */
    fun loadMore(offset: Int) {
        // More data should not be fetched while a fetching has already requested and not completed.
        if (_uiState.value != SchoolUIState.Loading) {
            loadSchools(offset)
        }
    }

    /**
     * This method loads the schools. Basically uses Rx-java to fetches the schools
     * by requesting from network.
     * @param offset This number of first element should be fetched. initial to zero
     * for the first fetch.
     */
    private fun loadSchools(offset: Int = 0) {
        compositeDisposable += schoolRepository.getSchools(offset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _uiState.value = SchoolUIState.Loading
            }
            .subscribeBy(
                onError = {
                    _uiState.value = SchoolUIState.Error(it)
                },
                onSuccess = {
                    _uiState.value = SchoolUIState.Success(
                        schoolMapper.map(it)
                    )
                }
            )
    }
}