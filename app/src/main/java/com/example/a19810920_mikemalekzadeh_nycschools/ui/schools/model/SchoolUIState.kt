package com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model

import com.google.gson.annotations.SerializedName

sealed class SchoolUIState {
    object Loading: SchoolUIState()
    data class Error(val throwable: Throwable): SchoolUIState()
    data class Success(val schoolPresentationModel: SchoolPresentationModel): SchoolUIState()
}

data class SchoolPresentationModel(
    val school: List<School>
) {
    data class School(
        val dbn: String,
        val schoolName: String,
        val city: String,
        val website: String,
        val schoolEmail: String
    )
}