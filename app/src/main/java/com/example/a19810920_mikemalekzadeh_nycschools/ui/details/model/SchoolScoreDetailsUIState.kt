package com.example.a19810920_mikemalekzadeh_nycschools.ui.details.model

sealed class SchoolScoreDetailsUIState {
    object Loading : SchoolScoreDetailsUIState()
    data class Error(val message: String) : SchoolScoreDetailsUIState()
    data class Success(val schoolScoreDetailsPresentationModel: SchoolScoreDetailsPresentationModel) :
        SchoolScoreDetailsUIState()
}

data class SchoolScoreDetailsPresentationModel(
    val dbn: String,
    val schoolName: String,
    val numOfSatTestTakersText: String,
    val satCriticalReadingAvgScoreText: String,
    val satMathAvgScoreText: String,
    val satWritingAvgScoreText: String
)
