package com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.adapter

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.a19810920_mikemalekzadeh_nycschools.R
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.model.SchoolPresentationModel

/**
 * Recycler adapter class that is set to Recycler view.
 * @param onClicked passed and be invoked in the activity with dbn.
 */
class SchoolAdapter constructor(
    private val onClicked: (dbn: String) -> Unit,
    private val onWebSiteClicked: (url: String) -> Unit
) : RecyclerView.Adapter<SchoolAdapter.SchoolItemViewHolder>() {

    private val data = ArrayList<SchoolPresentationModel.School>()

    inner class SchoolItemViewHolder constructor(itemView: View) : ViewHolder(itemView) {
        private val schoolNameTextview: TextView = itemView.findViewById(R.id.schoolNameTextview)
        private val cityTextview: TextView = itemView.findViewById(R.id.cityTextview)
        private val websiteTextview: TextView = itemView.findViewById(R.id.websiteTextview)
        private val emailTextview: TextView = itemView.findViewById(R.id.emailTextview)

        fun bind(school: SchoolPresentationModel.School) {
            schoolNameTextview.text = school.schoolName
            cityTextview.text = school.city
            websiteTextview.text = school.website
            websiteTextview.movementMethod = LinkMovementMethod.getInstance()
            websiteTextview.setOnClickListener {
                onWebSiteClicked.invoke(school.website)
            }
            emailTextview.text = school.schoolEmail
            itemView.setOnClickListener {
                onClicked.invoke(school.dbn)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder {
        return SchoolItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.school_item_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SchoolItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    fun update(schools: List<SchoolPresentationModel.School>) {
        data.addAll(schools)
        notifyDataSetChanged()
    }
}


