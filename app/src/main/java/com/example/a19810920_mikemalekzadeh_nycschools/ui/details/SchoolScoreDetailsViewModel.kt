package com.example.a19810920_mikemalekzadeh_nycschools.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.a19810920_mikemalekzadeh_nycschools.repository.SchoolRepository
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.mapper.SchoolScoreDetailsMapper
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.model.SchoolScoreDetailsUIState
import com.example.a19810920_mikemalekzadeh_nycschools.ui.schools.SchoolListActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * This view model is used to fetch score details for a school.
 * @param schoolRepository repository for fetch school details
 * @param schoolScoreDetailsMapper map domain model to presentation model
 */
@HiltViewModel
class SchoolScoreDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val schoolRepository: SchoolRepository,
    private val schoolScoreDetailsMapper: SchoolScoreDetailsMapper
) : ViewModel() {

    /**
     * This is a disposable that keeps all observable and clear those
     * whenever the ViewModel is destroyed.
     */
    private val compositeDisposable = CompositeDisposable()

    /**
     * Using Live data to post the UI States. There UI states here
     * Loading, Error, Success.
     */
    private val _uiState = MutableLiveData<SchoolScoreDetailsUIState>()
    val uiState: LiveData<SchoolScoreDetailsUIState> = _uiState


    /**
     * This pass the dbn to from [SchoolListActivity]
     */
    val dbn = savedStateHandle.get<String>(SchoolScoreDetailsActivity.SCHOOL_DBN)
        ?: throw NullPointerException("DBN is null")


    init {
        // Places initial fetch as soon as viewModel is initialized.
        loadSchoolDetails()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun loadSchoolDetails() {
        compositeDisposable += schoolRepository.getSchoolScoreDetails(dbn)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _uiState.value = SchoolScoreDetailsUIState.Loading
            }
            .subscribeBy(
                onError = {
                    _uiState.value = SchoolScoreDetailsUIState.Error(it.stackTraceToString())
                },
                onSuccess = {
                    if (it.isEmpty()) {
                        _uiState.value = SchoolScoreDetailsUIState.Error("Details not available")
                    } else {
                        _uiState.value = SchoolScoreDetailsUIState.Success(
                            schoolScoreDetailsMapper.map(it)
                        )
                    }

                }
            )
    }
}