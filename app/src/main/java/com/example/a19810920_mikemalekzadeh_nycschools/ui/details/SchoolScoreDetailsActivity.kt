package com.example.a19810920_mikemalekzadeh_nycschools.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.a19810920_mikemalekzadeh_nycschools.R
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.model.SchoolScoreDetailsPresentationModel
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.model.SchoolScoreDetailsUIState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolScoreDetailsActivity : AppCompatActivity() {

    companion object {
        const val SCHOOL_DBN = "school_dbn"

        fun getIntent(context: Context, dbn: String): Intent {
            return Intent(context, SchoolScoreDetailsActivity::class.java)
                .also {
                    val bundle = Bundle()
                    bundle.putString(SCHOOL_DBN, dbn)
                    it.putExtras(bundle)
                }
        }
    }

    private val viewModel: SchoolScoreDetailsViewModel by viewModels()
    private lateinit var progressBar: ProgressBar
    private lateinit var schoolNameTextview: TextView
    private lateinit var numOfSatTestTakersTextview: TextView
    private lateinit var satCriticalReadingAvgScoreTextview: TextView
    private lateinit var satMathAvgScoreTextview: TextView
    private lateinit var satWritingAvgScoreTextview: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_score_details)

        setupViews()
        observeToUiState()
    }

    private fun setupViews() {
        progressBar = findViewById(R.id.progressBar)
        schoolNameTextview = findViewById(R.id.schoolNameTextview)
        numOfSatTestTakersTextview = findViewById(R.id.numOfSatTestTakersTextview)
        satCriticalReadingAvgScoreTextview = findViewById(R.id.satCriticalReadingAvgScoreTextview)
        satMathAvgScoreTextview = findViewById(R.id.satMathAvgScoreTextview)
        satWritingAvgScoreTextview = findViewById(R.id.satWritingAvgScoreTextview)
    }

    private fun observeToUiState() {
        viewModel.uiState.observe(this) { schoolDetailsUIState ->
            handleUiState(schoolDetailsUIState)
        }
    }

    private fun handleUiState(uiState: SchoolScoreDetailsUIState) {
        when (uiState) {
            SchoolScoreDetailsUIState.Loading -> {
                progressBar.isVisible = true
            }
            is SchoolScoreDetailsUIState.Error -> {
                progressBar.isVisible = false
                Toast.makeText(this, uiState.message, Toast.LENGTH_LONG)
                    .show()
            }
            is SchoolScoreDetailsUIState.Success -> {
                progressBar.isVisible = false
                renderUi(uiState.schoolScoreDetailsPresentationModel)
            }
        }
    }

    private fun renderUi(schoolScoreDetailsPresentationModel: SchoolScoreDetailsPresentationModel) {
        with(schoolScoreDetailsPresentationModel) {
            schoolNameTextview.text = schoolName
            numOfSatTestTakersTextview.text = numOfSatTestTakersText
            satCriticalReadingAvgScoreTextview.text = satCriticalReadingAvgScoreText
            satMathAvgScoreTextview.text = satMathAvgScoreText
            satWritingAvgScoreTextview.text = satWritingAvgScoreText
        }
    }


}