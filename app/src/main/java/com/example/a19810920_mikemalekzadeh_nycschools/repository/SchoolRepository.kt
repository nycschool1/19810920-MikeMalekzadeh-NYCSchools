package com.example.a19810920_mikemalekzadeh_nycschools.repository

import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolScoreDetailsResponse
import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolResponse
import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolService
import com.example.a19810920_mikemalekzadeh_nycschools.network.cache.Cache
import io.reactivex.Single
import javax.inject.Inject

/**
 * This is a repository that fetch the data through school service
 * @param schoolService It's a retrofit service interface
 * @param cache This is a memory cache to store school score. There is a chance user click couple of
 * time to see the details of one school. With cache the data stored in memory.
 */
class SchoolRepository @Inject constructor(
    private val schoolService: SchoolService,
    private val cache: Cache
) {

    companion object {
        // Limit is 50 by default so only 50 items are fetched for each network call.
        private const val LIMIT = 50

        // Since the payload might be huge, it only query and filter
        // "dbn,school_name,city,website,school_email" I used those fields make this app.
        private const val QUERY = "dbn,school_name,city,website,school_email"

        // This make the order ascending
        private const val ORDER = ":id+ASC"
    }

    /**
     * fetches list of schools
     * @param offset Offset for pagination.
     */
    fun getSchools(offset: Int): Single<List<SchoolResponse>> {

        // By default only 50 items are fetched from network service.
        return schoolService.getSchools(
            select = QUERY,
            order = ORDER,
            limit = LIMIT,
            offset = offset
        )
    }

    /**
     * Fetches Sat score for a school.
     * @param dbn dbn number for a school.
     */
    fun getSchoolScoreDetails(dbn: String): Single<List<SchoolScoreDetailsResponse>> {
        // Checks the cache and if the response is available it returns from cache
        return if (cache.isValid(dbn)) {
            return Single.just(cache.get(dbn) as List<SchoolScoreDetailsResponse>)
        } else {
            schoolService.getSchoolScoreDetails(dbn)
                .map {
                    cache.add(dbn, it)
                    it
                }
        }
    }

}