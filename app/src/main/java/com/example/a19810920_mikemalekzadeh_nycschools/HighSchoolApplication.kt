package com.example.a19810920_mikemalekzadeh_nycschools

import dagger.hilt.android.HiltAndroidApp
import android.app.Application

/**
 * This is main application. This needs to be defined and annotated with @HiltAndroidApp
 * since Dagger Hilt library is used.
 */
@HiltAndroidApp
class HighSchoolApplication : Application()