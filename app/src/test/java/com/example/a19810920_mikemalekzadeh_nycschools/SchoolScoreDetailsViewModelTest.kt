package com.example.a19810920_mikemalekzadeh_nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.example.a19810920_mikemalekzadeh_nycschools.repository.SchoolRepository
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.SchoolScoreDetailsActivity
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.SchoolScoreDetailsViewModel
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.mapper.SchoolScoreDetailsMapper
import com.example.a19810920_mikemalekzadeh_nycschools.ui.details.model.SchoolScoreDetailsUIState
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.kotlin.*

class SchoolScoreDetailsViewModelTest {

    @get:Rule
    val schedulers = RxImmediateSchedulerRule()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mockSavedStateHandle = mock<SavedStateHandle>()
    private val mockSchoolRepository = mock<SchoolRepository>()
    private val mockSchoolScoreDetailsUIStateObserver = mock<Observer<SchoolScoreDetailsUIState>>()
    private val argumentSchoolScoreDetailsUIState = argumentCaptor<SchoolScoreDetailsUIState>()

    private val viewModel: SchoolScoreDetailsViewModel by lazy {
        SchoolScoreDetailsViewModel(
            mockSavedStateHandle,
            mockSchoolRepository,
            SchoolScoreDetailsMapper()
        )
    }

    @Before
    fun setup() {
        whenever(mockSavedStateHandle.get<String>(SchoolScoreDetailsActivity.SCHOOL_DBN)).thenReturn(
            "3o87"
        )
        val response = getSchoolScoreDetails()
        whenever(mockSchoolRepository.getSchoolScoreDetails("3o87")).thenReturn(Single.just(response))
    }

    @Test
    fun `verify repository is called`() {
        viewModel.uiState.observeForever(mockSchoolScoreDetailsUIStateObserver)

        argumentSchoolScoreDetailsUIState.run {
            verify(mockSchoolScoreDetailsUIStateObserver, times(1)).onChanged(capture())

            Assert.assertTrue(lastValue is SchoolScoreDetailsUIState.Success)
            Assert.assertEquals(
                "3o87", (lastValue as SchoolScoreDetailsUIState.Success)
                    .schoolScoreDetailsPresentationModel.dbn
            )
        }
    }
}