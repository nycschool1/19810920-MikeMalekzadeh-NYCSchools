package com.example.a19810920_mikemalekzadeh_nycschools

import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolResponse
import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolScoreDetailsResponse

fun getSchoolResponse() = listOf(
    SchoolResponse(
        dbn = "2983",
        schoolName = "test",
        city = "Manhatan",
        website = "www.test.com",
        schoolEmail = "school@email.com"
    ),
    SchoolResponse(
        dbn = "298334",
        schoolName = "test1",
        city = "San Francisco",
        website = "www.test1.com",
        schoolEmail = "school1@email.com"
    )
)

fun getSchoolScoreDetails() = listOf(
    SchoolScoreDetailsResponse(
        dbn = "3o87",
        schoolName = "test",
        numOfSatTestTakers = 233,
        satCriticalReadingAvgScore = 232,
        satMathAvgScore = 2323,
        satWritingAvgScore = 2323
    )
)