package com.example.a19810920_mikemalekzadeh_nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a19810920_mikemalekzadeh_nycschools.network.SchoolService
import com.example.a19810920_mikemalekzadeh_nycschools.network.cache.Cache
import com.example.a19810920_mikemalekzadeh_nycschools.repository.SchoolRepository
import io.reactivex.Single
import org.junit.Test

import org.junit.Rule
import org.junit.rules.TestRule
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 */
class SchoolRepositoryTest {

    @get:Rule
    val schedulers = RxImmediateSchedulerRule()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mockSchoolService = mock<SchoolService>()
    private val mockCache = mock<Cache>()

    private val schoolRepository = SchoolRepository(
        mockSchoolService,
        mockCache
    )

    @Test
    fun `verify get school list is called and return result`() {
        val response = getSchoolResponse()
        whenever(mockSchoolService.getSchools(any(), any(), any(), any()))
            .thenReturn(Single.just(response))

        schoolRepository.getSchools(123)
            .test()
            .assertResult(response)
            .assertComplete()

        verify(mockSchoolService).getSchools(any(), any(), any(), any())
    }

    @Test
    fun `verify get school score details is called and return result`() {
        val response = getSchoolScoreDetails()
        whenever(mockSchoolService.getSchoolScoreDetails(any()))
            .thenReturn(Single.just(response))
        whenever(mockCache.isValid(any())).thenReturn(false)

        schoolRepository.getSchoolScoreDetails("test")
            .test()
            .assertResult(response)
            .assertComplete()

        verify(mockCache).isValid(any())
        verify(mockSchoolService).getSchoolScoreDetails(any())

    }
}