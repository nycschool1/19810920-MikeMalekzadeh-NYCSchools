package com.example.a19810920_mikemalekzadeh_nycschools

import com.example.a19810920_mikemalekzadeh_nycschools.network.cache.Cache
import com.example.a19810920_mikemalekzadeh_nycschools.network.cache.CacheImpl
import org.junit.Assert.assertEquals
import org.junit.Test

class CacheTest {

    companion object {
        const val MAX_CACHE_SIZE = 3
    }

    @Test
    fun test_isValid() {
        val cache: Cache = CacheImpl(MAX_CACHE_SIZE)
        assertEquals(false, cache.isValid(key = "test"))

        cache.add("test_key1", "value")
        assertEquals(false, cache.isValid(key = "test"))
        assertEquals(true, cache.isValid(key = "test_key1"))

        cache.add("test_key1", null)
        assertEquals(true, cache.isValid(key = "test_key1"))

        cache.add("test_key2", "value2")
        cache.add("test_key3", "value3")
        cache.add("test_key4", "value4")
        cache.add("test_key5", "value5")
        cache.add("test_key6", "value6")
        assertEquals(false, cache.isValid(key = "test_key1"))
        assertEquals(true, cache.isValid(key = "test_key6"))
    }

    @Test
    fun test_cache_get() {
        val cache: Cache = CacheImpl(MAX_CACHE_SIZE)
        assertEquals(null, cache.get(key = "test"))

        cache.add("test_key1", "value")
        assertEquals(null, cache.get(key = "test"))
        assertEquals("value", cache.get(key = "test_key1"))

        cache.add("test_key1", null)
        assertEquals(null, cache.get(key = "test_key1"))

        cache.add("test_key2", "value2")
        cache.add("test_key3", "value3")
        cache.add("test_key4", "value4")
        cache.add("test_key5", "value5")
        cache.add("test_key6", "value6")
        assertEquals(null, cache.get(key = "test_key1"))
        assertEquals("value6", cache.get(key = "test_key6"))
        assertEquals("value3", cache.get(key = "test_key3"))
    }
}